import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logo from '../../../images/nameLogo.png';
import './navBar.css';

class NavBar extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} classname="App-logo" alt="logo" />
          <Link to="">About</Link>
        </header>
      </div>
    );
  }
}

export default NavBar;
