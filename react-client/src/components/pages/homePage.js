import React, { Component } from 'react';
//import logo from '../logo.svg';
import './homePage.css';
import pic from '../../../images/pic.png';

class HomePage extends Component {
  render() {
    return (
      <div className="Bio">
        <header className="Bio-header">
          <h1 className="Bio-title">Web Developer</h1>
        </header>
        <body className="Bio-body">
          <p className="Bio-intro">Hello, I'm Sheerlin </p>
        </body>
        <picture classname="pic">
          <img src={pic} />
        </picture>
      </div>
    );
  }
}

export default HomePage;
