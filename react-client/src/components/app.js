import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { browserHistory } from 'react-router';
import HomePage from './pages/homePage.js';
import NavBar from './headerComponent/navBar.js';
import Footer from './footerComponent/footer.js';
import './app.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="wrapper">
          <Route
            className="box header"
            name="nav"
            ecact
            path="/"
            component={NavBar}
          />
          <Route
            className="box content"
            name="home"
            exact
            path="/"
            component={HomePage}
          />
          <Footer className="box footer" />
        </div>
      </Router>
    );
  }
}

export default App;
